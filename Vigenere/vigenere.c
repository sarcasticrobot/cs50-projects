#include <stdio.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>

void encrypt_vigenere();

int main(int argc, string argv[])
{
    // check if valid key exists
    if (argc != 2)
    {
        printf("You need to add one command-line argument with only letters, for example \"./vigenere apples\", but not \"./vigenere apples!\"\n");
        return 1;
    }
    for(int k = 0, m = strlen(argv[1]); k < m; k++)
    {
        if ((argv[1][k] > 'Z' && argv[1][k] < 'a') || (argv[1][k] < 'A') || (argv[1][k] > 'z'))
        {
            printf("You need to add one command-line argument with only letters, for example \"./vigenere apples\", but not \"./vigenere apples!\"\n");
            return 1;
        }
    }
    // if argv[1] is a vaild key, encrypt
    encrypt_vigenere(argv[1]);
}

void encrypt_vigenere(string key)
{
    string plaintext;
    do
    {
        printf("plaintext: ");
        plaintext = get_string();
    }
    while (plaintext == NULL);
    int keylen = strlen(key);
    int letters = 26;
    int j = 0;
    printf("ciphertext: ");
    for(int i = 0, n = strlen(plaintext); i < n; i++)
    {
        if ((plaintext[i] >= 'A' && plaintext[i] <= 'Z') || (plaintext[i] >= 'a' && plaintext[i] <= 'z'))
        {
            // j counts only when its actual letters, the rest ensures that I cycle only letters
            char c = ((toupper(plaintext[i]) + toupper(key[j % keylen])) % letters) + 'A';
            if (islower(plaintext[i]))
            {
                printf("%c", tolower(c));
            }
            else
            {
                printf("%c", c);
            }
            j += 1;
        }
        else
        {
            printf("%c", plaintext[i]);
        }
    }
    printf("\n");
}