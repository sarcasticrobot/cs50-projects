/**
 * helpers.c
 *
 * Helper functions for Problem Set 3.
 */
 
#include <cs50.h>
#include <stdio.h>
#include "helpers.h"

/**
 * Returns true if value is in array of n values, else false.
 */

int mid;
int min;
int max;
int h = 0;

bool search(int value, int values[], int n)
{
    // first check if n is valid
    
    if (n < 0)
    {
        return false;
    }

    // this is so that mid, min, and max are only set the first time
    
    if (h == 0)
    {
        mid = (n - 1)/2;
        printf("mid is %i, n is %i\n", mid, n);
        min = 0;
        max = n - 1;
        h = 1;
    }
    else
    {
        printf("mid is %i, n is %i\n", mid, n);
    }
    
    // check if value is found
    
    if (values[mid] == value)
    {
        return true;
    }
    
    // check if value is left or right
    
    else if (values[mid] > value)
    {
        // left
        max = mid - 1;
        mid = (min + max)/2;
        
        // make sure that n does not get weird or repeat if n is already 1
        
        if (n > 1)
        {
            n = max + 1 - min;
            return search(value, values, n);
        }
        else if (n == 1)
        {
            return false;
        }
    }
    else if (values[mid] < value)
    {
        // right
        min = mid + 1;
        mid = (min + max)/2;
        if (n > 1)
        {
            n = max + 1 - min;
            return search(value, values, n);
        }
        else if (n == 1)
        {
            return false;
        }
    }
    return false;
}

/**
 * Sorts array of n values.
 */

void sort(int values[], int n)
{
    int index[65536] = {0}; // max number is 65535, as given by problem, so index is that plus one
    for (int e = 0; e < n; e++)
    {
        index[values[e]] += 1;
    }
    int i = 0;
    int j = 0;
    do
    {
        if (index[i] != 0)
        {
            values[j] = i;
            j += 1;
            index[i] -= 1;
        }
        else
        {
            i += 1;
        }
    }
    while (j < n);
}