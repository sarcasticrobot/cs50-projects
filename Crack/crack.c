#include <stdio.h>
#include <cs50.h>
#include <ctype.h>
#include <string.h>
#define _XOPEN_SOURCE
#include <unistd.h>

char *crypt(const char *key, const char *salt);

int crack(string hash, string salt);

int main(int argc, string argv[])
{
    if (argc != 2)
    {
        printf("Need a hash to crack, idiot.\n");
        return 1;
    }
    
    // string made is first two chars of hash, the salt for crypt function
    
    char fub[4];
    sprintf(fub, "%c%c", argv[1][0], argv[1][1]);
    crack(argv[1], fub);
}

// define crack password function

int crack(string hash, string salt)
{
    char buf[8];
    
    // combination creator for passwords
    
    for (int i = 65; i < 123; i++)
    {
        sprintf(buf, "%c", i);
        if (strncmp(crypt(buf, salt), hash, 13) == 0)
        {
            printf("%s\n", buf);
            return 0;
        }
        for (int j = 65; j < 123; j++)
        {
            sprintf(buf, "%c%c", i, j);
            if (strncmp(crypt(buf, salt), hash, 13) == 0)
            {
                printf("%s\n", buf);
                return 0;
            }
            for (int k = 65; k < 123; k++)
            {
                sprintf(buf, "%c%c%c", i, j, k);
                if (strncmp(crypt(buf, salt), hash, 13) == 0)
                {
                    printf("%s\n", buf);
                    return 0;
                }
                for (int l = 65; l < 123; l++)
                {
                    sprintf(buf, "%c%c%c%c", i, j, k, l);
                    if (strncmp(crypt(buf, salt), hash, 13) == 0)
                    {
                        printf("%s\n", buf);
                        return 0;
                    }
                }
            }
        }
    }
    printf("Did not work.\n");
    return 1;
}