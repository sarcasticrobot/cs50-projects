/**
 * Resize a bitmap file by a factor between 0 and 100.
 */
       
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

int main(int argc, char *argv[])
{
    // ensure proper usage
    if (argc != 4)
    {
        fprintf(stderr, "Usage: ./resize factor infile outfile\n");
        return 1;
    }

    // remember filenames
    char *infile = argv[2];
    char *outfile = argv[3];
    
    // store factor as a float instead of a char *, and check if in range
    float f = strtof(argv[1], NULL);
    if (strtof(argv[1], NULL) == 0 || f <= 0 || f > 100)
    {
        fprintf(stderr, "Usage: ./resize factor infile outfile\nFactor must be a number bigger than zero but not bigger than 100\n");
        return 1;
    }

    // open input file 
    FILE *inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", infile);
        return 2;
    }

    // open output file
    FILE *outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fclose(inptr);
        fprintf(stderr, "Could not create %s.\n", outfile);
        return 3;
    }

    // read infile's BITMAPFILEHEADER
    BITMAPFILEHEADER bf;
    fread(&bf, sizeof(BITMAPFILEHEADER), 1, inptr);

    // read infile's BITMAPINFOHEADER
    BITMAPINFOHEADER bi;
    fread(&bi, sizeof(BITMAPINFOHEADER), 1, inptr);

    // ensure infile is (likely) a 24-bit uncompressed BMP 4.0
    if (bf.bfType != 0x4d42 || bf.bfOffBits != 54 || bi.biSize != 40 || 
        bi.biBitCount != 24 || bi.biCompression != 0)
    {
        fclose(outptr);
        fclose(inptr);
        fprintf(stderr, "Unsupported file format.\n");
        return 4;
    }
    
    // create bf and bi for outfile
    BITMAPFILEHEADER bfout = bf;
    BITMAPINFOHEADER biout = bi;
    biout.biWidth = f*bi.biWidth;
    biout.biHeight = f*bi.biHeight;
    
    // determine padding for scanlines
    int padding = (4 - (bi.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    int out_padding = (4 - (biout.biWidth * sizeof(RGBTRIPLE)) % 4) % 4;
    
    bfout.bfSize = bf.bfSize - (bi.biWidth + padding) * abs(bi.biHeight) * 3 + (biout.biWidth + out_padding) * abs(biout.biHeight) * 3;
    
    fwrite(&bfout, sizeof(BITMAPFILEHEADER), 1, outptr);
    fwrite(&biout, sizeof(BITMAPINFOHEADER), 1, outptr);
    printf("size is %i, width is %i, height is %i\n", bfout.bfSize, biout.biWidth, biout.biHeight);

    // storing f as integer part and as only the part after the decimal point
    int f_int = f;
    printf("f_int is %i\n", f_int);
    float f_part = f - f_int;
    printf("f_part is %f\n", f_part);
    
    // variable for storing pixel array for outfile
    RGBTRIPLE out_pixels[biout.biWidth][abs(biout.biHeight)];
    
    // variables for knowing when to add pixels to width or height
    float count_x = f_part;
    float count_y = f_part;
    
    // iterate over infile's scanlines
    int add_y = 0;
    for (int i = 0, biHeight = abs(bi.biHeight); i < biHeight; i++)
    {
        int add_x = 0;
        count_x = f_part;
        for (int j = 0; j < bi.biWidth; j++)
        {
            // temporary storage
            RGBTRIPLE triple;

            // read RGB triple from infile
            fread(&triple, sizeof(RGBTRIPLE), 1, inptr);

            // write RGB triple to out_pixels
            for (int row = f_int*i + add_y; row < f_int*(i + 1) + add_y + (int)count_y; row++)
            {
                for (int col = f_int*j + add_x; col < f_int*(j + 1) + add_x + (int)count_x; col++)
                {
                    out_pixels[row][col] = triple;
                }
            }
            if (count_x >= 1)
            {
                add_x += 1;
                count_x -= 1;
            }
            count_x += f_part;
        }
        // skip over padding, if any
        fseek(inptr, padding, SEEK_CUR);
        
        if (count_y >= 1)
        {
            add_y += 1;
            count_y -= 1;
        }
        count_y += f_part;
    }
    
    for (int a = 0, bioutHeight = abs(biout.biHeight); a < bioutHeight; a++)
    {
        for (int b = 0; b < biout.biWidth; b++)
        {
            fwrite(&out_pixels[a][b], sizeof(RGBTRIPLE), 1, outptr);
        }
        for (int k = 0; k < out_padding; k++)
        {
            fputc(0x00, outptr);
        }
    }
    
    // close infile
    fclose(inptr);

    // close outfile
    fclose(outptr);

    // success
    return 0;
}
