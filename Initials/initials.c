#include <cs50.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

void initials();

int main(void)
{
    initials();
}

void initials(void)
{
    string fullname;
    do
    {
        fullname = get_string();
    }
    while (fullname == NULL);
    //boolean operator for whether I have already done the intial for a name
    bool finished = false;
    //iterate over characters in the string
    for (int i = 0, n = strlen(fullname); i < n; i++)
    {
        //find a non-space character
        if ((fullname[i] != (char) 32) && (finished == false))
        {
            //print it capitalized and set the name as finished
            printf("%c", toupper(fullname[i]));
            finished = true;
        }
        //finds a space to indicate that we have gotten to the next name
        else if (fullname[i] == (char) 32)
        {
            finished = false;
        }
    }
    printf("\n");
}