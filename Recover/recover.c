/**
 * Recover 
 */
       
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: ./recover filename\n");
        return 1;
    }

    char *infile = argv[1];
    FILE *inptr = fopen(infile, "r");
    if (inptr == NULL)
    {
        fprintf(stderr, "Could not open %s.\n", infile);
        return 2;
    }

    // stuff happening starts here
    
    uint8_t block[512];
    
    int images = 0;
    char outfile[12];
    sprintf(outfile, "Pic %i.jpg", images);
    FILE *outptr = fopen(outfile, "w");
    if (outptr == NULL)
    {
        fprintf(stderr, "Could not open file %s.\n", outfile);
        return 2;
    }
    
    // to check for end of the file
    while (!(feof(inptr)))
    {
        // check for signature, and if found write to a new image and close old image
        fread(&block, 1, 4, inptr);
        if (block[0] == 0xff && block[1] == 0xd8 && block[2] == 0xff && !(block[3] < 0xe0 || block[3] > 0xef))
        {
            fclose(outptr);
            if (images == 0)
            {
                remove(outfile);
            }
            images += 1;
            fseek(inptr, -4, SEEK_CUR);
            fread(&block, 1, 512, inptr);
            sprintf(outfile, "Pic %i.jpg", images);
            outptr = fopen(outfile, "w");
            if (outptr == NULL)
            {
                fprintf(stderr, "Could not open file %s.\n", outfile);
                break;
            }
            fwrite(&block, 1, 512, outptr);
        }
        // if not image signature, either continue writing the block or seek to find the first image if no images have yet been found
        else
        {
            if (images >= 1)
            {
                fseek(inptr, -4, SEEK_CUR);
                fread(&block, 1, 512, inptr);
                fwrite(&block, 1, 512, outptr);
            }
            else
            {
                fseek(inptr, 508, SEEK_CUR);
            }
        }
    }
    
    return 0;
}
