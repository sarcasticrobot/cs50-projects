#include <stdio.h>
#include <cs50.h>

void pyramid(int height);
void askheight(void);

int main(void)
{
    askheight();
    int height = get_int();
    if (height <= 23 && height >= 0)
    {
        pyramid(height);
    }
    else
    {
        main();
    }
}

void pyramid(int height)
{
    for(int i = 0; i < height; i++)
    {
        for(int j = i; j < height - 1; j++)
        {
            printf(" ");
        }
        for(int k = 0; k < i + 1; k++)
        {
            printf("#");
        }
        printf("  ");
        for(int l = 0; l < i + 1; l++)
        {
            printf("#");
        }
        printf("\n");
    }
}

void askheight(void)
{
    printf("Height: ");
}