// Testing

#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>

// max word length
#define LENGTH 45

// number of characters that a dictionary can contain
#define CHARACTERS 27 // alphabet plus apostrophe

bool load(const char *dictionary);

// node for trie letters
typedef struct trie
{
    bool exist;
    struct trie *letters[CHARACTERS];
}
trie;

int main(int argc, char *argv[])
{
    // alert errors
    if (argc != 3)
    {
        printf("Error 1");
        return 1;
    }
    
    load(argv[1]);
    char *text = (argc == 3) ? argv[2] : argv[1];
    FILE *fp = fopen(text, "r");
    if (fp == NULL)
    {
        printf("Could not open %s.\n", text);
        unload();
        return 1;
    }

    // prepare to report misspellings
    printf("\nMISSPELLED WORDS\n\n");

    // prepare to spell-check
    int index = 0, misspellings = 0, words = 0;
    char word[LENGTH+1];
    for (int c = fgetc(fp); c != EOF; c = fgetc(fp))
    {
        // allow only alphabetical characters and apostrophes
        if (isalpha(c) || (c == '\'' && index > 0))
        {
            // append character to word
            word[index] = c;
            index++;

            // ignore alphabetical strings too long to be words
            if (index > LENGTH)
            {
                // consume remainder of alphabetical string
                while ((c = fgetc(fp)) != EOF && isalpha(c));

                // prepare for new word
                index = 0;
            }
        }

        // ignore words with numbers (like MS Word can)
        else if (isdigit(c))
        {
            // consume remainder of alphanumeric string
            while ((c = fgetc(fp)) != EOF && isalnum(c));

            // prepare for new word
            index = 0;
        }

        // we must have found a whole word
        else if (index > 0)
        {
            // terminate current word
            word[index] = '\0';

            // update counter
            words++;

            // check word's spelling
            getrusage(RUSAGE_SELF, &before);
            bool misspelled = !check(word);
            getrusage(RUSAGE_SELF, &after);

            // update benchmark
            time_check += calculate(&before, &after);

            // print word if misspelled
            if (misspelled)
            {
                printf("%s\n", word);
                misspellings++;
            }
}

// convert txt dictionary into a trie structure
bool load(const char *dictionary)
{
    FILE *in = fopen(dictionary, "r");
    if (in == NULL)
    {
        printf("Could not open %s.\n", dictionary);
        return false;
    }
    static trie *start = malloc(sizeof(trie));
    if (start == NULL)
    {
        printf("Dumbo\n", dictionary);
        return false;
    }
    trie *next = start
    int cur;
    for (int c = fgetc(in); c != EOF; c = fgetc(in))
    {
        if (isalnum(c))
        {
            if (strcmp(c, '\'') == 0)
            {
                // current letter of word, indexed as int
                cur = 0;
            }
            else
            {
                // because a-z is 97-122
                cur = c - 96;
            }
            // allocate its pointer and ensure it's not null
            next->letters[cur] = malloc(sizeof(trie));
            if (next->letters[cur] == NULL)
            {
                printf("oops\n");
                unload();
                return false;
            }
            // set that pointer as next pointer for iteration
            next = next->letters[cur];
            
        }
        else
        {
            // must have reached end of word
            // need to identify that this is a word using boolean
            next->exist = true;
            // now need to reset, since we will be starting a new word
            next = start;
        }
    }
    if (ferror(in))
    {
        fclose(in);
        printf("Error reading %s.\n", text);
        unload();
        return 1;
    }
}