/**
 * Implements a dictionary's functionality.
 */

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#include "dictionary.h"

trie *start;

static unsigned int total_size = 0;

/**
 * Returns true if word is in dictionary else false.
 */
bool check(const char *word)
{
    trie *next = start;
    int cur;
    for (int ch = 0, wordlen = strlen(word); ch < wordlen; ch++)
    {
        if (word[ch] == '\'')
        {
            // current letter of word, indexed as int
            cur = 0;
        }
        else
        {
            // because a-z is 97-122
            cur = tolower(word[ch]) - 96;
        }
        // check if pointer is just null and end it if so
        if (next->letters[cur] == NULL)
        {
            return false;
        }
        // otherwise set that pointer as next pointer for iteration
        next = next->letters[cur];
    }
    // need to identify whether this is a word using boolean
    return next->exist;
}

/**
 * Loads dictionary into memory. Returns true if successful else false.
 */
bool load(const char *dict)
{
    FILE *in = fopen(dict, "r");
    if (in == NULL)
    {
        printf("Could not open %s.\n", dict);
        return false;
    }
    start = malloc(sizeof(trie));
    if (start == NULL)
    {
        printf("abort\n");
        return false;
    }
    trie *next = start;
    int cur;
    for (int c = fgetc(in); c != EOF; c = fgetc(in))
    {
        if (isalnum(c) || c == '\'')
        {
            if (c == '\'')
            {
                // current letter of word, indexed as int
                cur = 0;
            }
            else
            {
                // because a-z is 97-122
                cur = c - 96;
            }
            // allocate its pointer if not already done and ensure it's not null
            if (next->letters[cur] == NULL)
            {
                next->letters[cur] = malloc(sizeof(trie));
                if (next->letters[cur] == NULL)
                {
                    printf("heyo\n");
                    return false;
                }
            }
            // set that pointer as next pointer for iteration
            next = next->letters[cur];
            
        }
        else
        {
            // must have reached end of word
            // need to identify that this is a word using boolean and add to words in dict
            next->exist = true;
            total_size++;
            // now need to reset, since we will be starting a new word
            next = start;
        }
    }
    if (ferror(in))
    {
        fclose(in);
        printf("Error reading dict somehow.\n");
        return false;
    }
    fclose(in);
    return true;
}

/**
 * Returns number of words in dictionary if loaded else 0 if not yet loaded.
 */
unsigned int size(void)
{
    return total_size;
}

/**
 * Assists in unloading dictionary
 */
bool search(trie *future)
{
    if (future == NULL)
    {
        return false;
    }
    for (int i = 0; i < 27; i++)
    {
        if (future->letters[i] != NULL)
        {
            search(future->letters[i]);
        }
    }
    free(future);
    return true;
}

/**
 * Unloads dictionary from memory. Returns true if successful else false.
 */
bool unload(void)
{
    return search(start);
}
