#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>

typedef struct news
{
    bool valid;
    struct news *paths[27];
}
news;

int main(void)
{
    news *start = malloc(sizeof(news));
    printf("%lu   %p\n", sizeof(news), start);
    free(start);
    return 0;
}